var fieldMappingHelper = {
    isProjecPFieldCFieldMapped : function (pKey, pFId, cFId) {
        var baseUrl = AJS.params.baseURL;
        jQuery.ajax({
            url:  baseUrl+"/rest/asplselect/1/mappings/check/"+pKey+"/"+pFId+"/"+cFId,
            type: 'get',
            dataType: 'json',
            async: false,
            success: function (data) {
                $('#fieldmappingform').trigger('submit');
            },
            error: function (err) {
                if(err.status === 409){
                    showError(err.responseText);
                }
            }
        })
    }
}

AJS.$(document).ready(function(){

    $(".edit-mapping-link").on('click', function (e) {
        e.preventDefault();

        var fieldMapData=$(this).attr("data")
        var fieldMap = jQuery.parseJSON(fieldMapData);
        console.log("fieldMap : ",fieldMap)

        $('#hd_p_key').val(fieldMap.project_key);
        $('#hd_f_m_id').val(fieldMap.field_mapping_id);
        $('#span-p-f-name').text(fieldMap.parent_field_name);
        $('#span-c-f-name').text(fieldMap.child_field_name);
        $('#hd_p_f_id').val(fieldMap.parent_field_id);
        $('#hd_c_f_id').val(fieldMap.child_field_id);
        AJS.dialog2("#edit-field-mapping-dialog").show();
    });


    $("#mapping-add-submit-button").on( "click", function(e) {
        var cField = $("#cField").val();
        var pField = $("#pField").val();
        var project = $("#project").val();

        if(pField === null || cField === null || pField === '' || cField === ''){
            showError("Parent Field or Child field can not be empty!!!")
            return false;
        }
        if(pField === cField){
            showError("Parent Field and Child field can not be same!!!")
            return false;
        }
        fieldMappingHelper.isProjecPFieldCFieldMapped(project, pField, cField);

        e.preventDefault();
    });

    $('#mapping-update-submit-button').on( "click", function() {
        var proceed=confirm("Are you sure you want to update project ?");
        if(proceed)
            return true;
        return false;
    });

    $(".delete-mapping-link").on( "click", function() {
        var proceed=confirm("Are you sure you want to delete field mapping ?");
        if(proceed)
            return true;
        return false;
    });

})

var showSuccess = function(message){
    $("#div-field-mapping-error").children("p").text(message);
    $("#div-field-mapping-error").removeClass("hidden").removeClass("aui-message-error");
    $("#div-field-mapping-error").addClass("aui-message-success");
    $("#div-field-mapping-error").show().delay(2000).fadeOut();
}

var showError = function(message){
    $("#div-field-mapping-error").children("p").text(message);
    $("#div-field-mapping-error").removeClass("hidden").removeClass("aui-message-success");
    $("#div-field-mapping-error").addClass("aui-message-error");
    $("#div-field-mapping-error").show().delay(2000).fadeOut();
}