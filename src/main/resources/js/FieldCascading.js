var noneOption = -1;
var pKey = "";

JIRA.bind(JIRA.Events.ISSUE_REFRESHED, function(e, context, reason) {
	var allProjectsMappings = getFieldMappings("allprojects", AJS.params.baseURL);
	
	if(JIRA.Issue.getIssueKey().split("-")[0] != "" || JIRA.Issue.getIssueKey().split("-")[0] != null){
		pKey = JIRA.Issue.getIssueKey().split("-")[0];
	}else{
		pKey = AJS.$("#key-val").text().split("-")[0];
	}
	
		var allMappings = getFieldMappings(pKey, AJS.params.baseURL);
		if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
			var interval = setInterval(function() {
				if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
					    childField = "#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
					    parentField = "#"+allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
					    if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
					    	clearInterval(interval);
					    }else{
					    	disableInlineEdit(parentField, childField);
					    }
					}
				}
				if(allProjectsMappings && allProjectsMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
					    childField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
					    parentField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
					    if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
					    	clearInterval(interval);
					    }else{
					    	disableInlineEdit(parentField, childField);
					    }
					}
				}
			}, 200);
					
			for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
				var parentOption =  $("#"+allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD).val();
				if(parentOption!=null){
					var mapping = getOptionMappings(pKey, allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD, parentOption, AJS.params.baseURL);
					if(mapping.FIELD_MAPPINGS.length != 0){
						if(mapping.FIELD_MAPPINGS[0].OPTIONS_MAPPINGS.length == 0 || mapping == undefined){
							showAllOptionsInChild(mapping.FIELD_MAPPINGS[0].CHILD_FIELD);
						}
						else{ 
							var optionsToShow = mapping.FIELD_MAPPINGS[0].OPTIONS_MAPPINGS[0].CHILD_OPTIONS_ID.split(",")	
							showSelectedOptionsInChild(mapping.FIELD_MAPPINGS[0].CHILD_FIELD,optionsToShow)	
						}
					}
				}
			}
		}
		
			
	if(allProjectsMappings.FIELD_MAPPINGS.length != 0){
		for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
			var parentOption = $("#"+allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD).val();
			if(parentOption!=null){
				var allMapping = getOptionMappings("allprojects", allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD, parentOption, AJS.params.baseURL);
				if(allMapping.FIELD_MAPPINGS.length != 0){
					for( maps=0; maps<mapping.FIELD_MAPPINGS.length; maps++) {
						if (allMapping.FIELD_MAPPINGS[maps].OPTIONS_MAPPINGS.length == 0 || allMapping == undefined) {
							showAllOptionsInChild(allMapping.FIELD_MAPPINGS[maps].CHILD_FIELD);
						} else {
							var optionsToShow = allMapping.FIELD_MAPPINGS[maps].OPTIONS_MAPPINGS[0].CHILD_OPTIONS_ID.split(",")
							showSelectedOptionsInChild(allMapping.FIELD_MAPPINGS[maps].CHILD_FIELD, optionsToShow)
						}
					}
				}
			}
		}
	}
		
});

JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context, reason) {
	if (reason == JIRA.CONTENT_ADDED_REASON.pageLoad || reason == JIRA.CONTENT_ADDED_REASON.dialogReady){
		var allProjectsMappings = getFieldMappings("allprojects", AJS.params.baseURL);
		if(AJS.$("#project").val() != null){
			pKey =getProjectKeyByPid(AJS.$("#project").val(), AJS.params.baseURL);
			var allMappings = getFieldMappings(pKey, AJS.params.baseURL);
			var interval = setInterval(function() { 
				if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
					    childField = "#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
					    parentField = "#"+allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
					    if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
					    	clearInterval(interval);
					    }else{
					    	disableInlineEdit(parentField, childField);
					    }
					}
				}
			}, 200);
				
			var interval = setInterval(function() {
				if(allProjectsMappings && allProjectsMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
					    childField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
					    parentField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
					    if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
					    	clearInterval(interval);
					    }else{
					    	disableInlineEdit(parentField, childField);
					    }
					}
				}
			}, 200);
		
			if(allProjectsMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
					$("#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD).find("option").each(function(){
						$(this).hide();
					});
				}
			}
					
			if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
					$("#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD).find("option").each(function(){
						$(this).hide();
					});
				}
			}
		}else if(AJS.$("input:hidden[name=pid]").val() != null){
			pKey =getProjectKeyByPid(AJS.$("input:hidden[name=pid]").val(), AJS.params.baseURL);
			var allMappings = getFieldMappings(pKey, AJS.params.baseURL);
			var interval = setInterval(function() { 
				if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
					    childField = "#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
					    parentField = "#"+allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
					    if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
					    	clearInterval(interval);
					    }else{
					    	disableInlineEdit(parentField, childField);
					    }
					}
				}
			}, 200);
				
			var interval = setInterval(function() {
				if(allProjectsMappings && allProjectsMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
					    childField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
					    parentField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
					    if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
					    	clearInterval(interval);
					    }else{
					    	disableInlineEdit(parentField, childField);
					    }
					}
				}
			}, 200);
		
			if(allProjectsMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
					$("#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD).find("option").each(function(){
						$(this).hide();
					});
				}
			}
					
			if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
					$("#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD).find("option").each(function(){
						$(this).hide();
					});
				}
			}
		}else if(AJS.$("#project-field").val() != null){
			var pKeyStr = AJS.$("#project-field").val();
			pKey = pKeyStr.substring(pKeyStr.lastIndexOf("(")+1, pKeyStr.lastIndexOf(")"));
			var allMappings = getFieldMappings(pKey, AJS.params.baseURL);
			var interval = setInterval(function() {
				if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
				    	childField = "#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
				    	parentField = "#"+allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
				    	if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
					    	clearInterval(interval);
					    }else{
					    	disableInlineEdit(parentField, childField);
					    }
					}
				}
			}, 200);
			var interval = setInterval(function() {
				if(allProjectsMappings && allProjectsMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
				    	 childField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
				    	 parentField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
				    	 if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
						    	clearInterval(interval);
						 }else{
						    disableInlineEdit(parentField, childField);
						}
					}
				}
			}, 200);
			if(allProjectsMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
					$("#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD).find("option").each(function(){
					    $(this).hide();
					});
				}
			}
			if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
					$("#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD).find("option").each(function(){
					    $(this).hide();
					});
				}
			}
		}else {
			if(JIRA.API.Projects.getCurrentProjectKey() != null){ //Runs on Issue View page and Issue Edit dialog
				pKey = JIRA.API.Projects.getCurrentProjectKey();
			}
			else if($("#project-name-val").length){	//Runs on Issue View page and Issue Edit dialog	navigated from Issue Navigator			
				var uri=$("#project-name-val").attr("href");
                pKey=uri.substring(uri.lastIndexOf('/')  + 1)
			}
			var allMappings = getFieldMappings(pKey, AJS.params.baseURL);
			var interval = setInterval(function() {
				if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
				    	 childField = "#"+allMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
				    	 parentField = "#"+allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
				    	 if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
						    clearInterval(interval);
						 }else{
						    disableInlineEdit(parentField, childField);
						}
					}
				}
			}, 200);
			var interval = setInterval(function() {
				if(allProjectsMappings && allProjectsMappings.FIELD_MAPPINGS.length != 0){
					for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
				    	 childField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].CHILD_FIELD+"-val";
				    	 parentField = "#"+allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD+"-val";
				    	 if(AJS.$(parentField).find('span.overlay-icon').is(":hidden") && AJS.$(childField).find('span.overlay-icon').is(":hidden")){
							 clearInterval(interval);
						 }else{
							 disableInlineEdit(parentField, childField);
						 }
					}
				}
			}, 200);
			if(allProjectsMappings && allProjectsMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allProjectsMappings.FIELD_MAPPINGS.length; fm++ ){
					var parentOption = $("#"+allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD).val();
					if(parentOption!=null)
					{
						var allMapping = getOptionMappings("allprojects", allProjectsMappings.FIELD_MAPPINGS[fm].PARENT_FIELD, parentOption, AJS.params.baseURL);
						if(allMapping.FIELD_MAPPINGS.length != 0){
							
							if(allMapping.FIELD_MAPPINGS[0].OPTIONS_MAPPINGS.length == 0 || allMapping == undefined || parentOption == noneOption)
							{
								showAllOptionsInChild(allMapping.FIELD_MAPPINGS[0].CHILD_FIELD);
							}
							else{ 
								var optionsToShow = allMapping.FIELD_MAPPINGS[0].OPTIONS_MAPPINGS[0].CHILD_OPTIONS_ID.split(",")	
								showSelectedOptionsInChild(allMapping.FIELD_MAPPINGS[0].CHILD_FIELD,optionsToShow)	
							}
						}
					}
				}
			}
			if(allMappings && allMappings.FIELD_MAPPINGS.length != 0){
				for(var fm = 0; fm < allMappings.FIELD_MAPPINGS.length; fm++ ){
					var parentOption =  $("#"+allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD).val();
					if(parentOption!=null){
						var mapping = getOptionMappings(pKey, allMappings.FIELD_MAPPINGS[fm].PARENT_FIELD, parentOption, AJS.params.baseURL);
						if(mapping.FIELD_MAPPINGS.length != 0){
							if(mapping.FIELD_MAPPINGS[0].OPTIONS_MAPPINGS.length == 0 || mapping == undefined || parentOption == noneOption){
								showAllOptionsInChild(mapping.FIELD_MAPPINGS[0].CHILD_FIELD);
							}
							else{ 
								var optionsToShow = mapping.FIELD_MAPPINGS[0].OPTIONS_MAPPINGS[0].CHILD_OPTIONS_ID.split(",")	
								showSelectedOptionsInChild(mapping.FIELD_MAPPINGS[0].CHILD_FIELD,optionsToShow)	
							}
						}
					}
				}
			}
		}
	
		AJS.$(".cf-select:not([multiple])").on("change",function(){
			var custId =  $(this).attr("id");
			var optionid = $(this).val();
			var maps;
			var optionsMapping;
			for( maps=0; maps<allProjectsMappings.FIELD_MAPPINGS.length; maps++){				
				if(allProjectsMappings.FIELD_MAPPINGS[maps].PARENT_FIELD==custId){
					optionsMapping = allProjectsMappings.FIELD_MAPPINGS[maps].OPTIONS_MAPPINGS.find(function(obj){
		                return obj.PARENT_OPTION_ID.toString() === optionid.toString()});					
					
					if(optionsMapping == undefined || optionid == noneOption){
						showAllOptionsInChild(allProjectsMappings.FIELD_MAPPINGS[maps].CHILD_FIELD);			
					}else{
						var optionsToShow = optionsMapping.CHILD_OPTIONS_ID.split(",");						
						showSelectedOptionsInChild(allProjectsMappings.FIELD_MAPPINGS[maps].CHILD_FIELD,optionsToShow)
					}
				}
				
			}
			var mapping = getOptionMappings(pKey, custId, optionid, AJS.params.baseURL);
			if(mapping.FIELD_MAPPINGS.length != 0){
				for( maps=0; maps<mapping.FIELD_MAPPINGS.length; maps++) {
					if (mapping.FIELD_MAPPINGS[maps].OPTIONS_MAPPINGS.length == 0 || mapping == undefined || optionid == noneOption) {
						showAllOptionsInChild(mapping.FIELD_MAPPINGS[maps].CHILD_FIELD);
					} else {
						var optionsToShow = mapping.FIELD_MAPPINGS[maps].OPTIONS_MAPPINGS[0].CHILD_OPTIONS_ID.split(",")
						showSelectedOptionsInChild(mapping.FIELD_MAPPINGS[maps].CHILD_FIELD, optionsToShow)
					}
				}
			}
		});
	}
});

function showAllOptionsInChild(childSelectCFID){
	$("#"+childSelectCFID).find("option").each(function(){
		$(this).show();
		$("#"+childSelectCFID+" option").show();							
	});
}

function showSelectedOptionsInChild(childSelectCFID, optionsToShow){
	$("#"+childSelectCFID).find("option").each(function(){
		var optionValue = $(this).val().trim();
		if(optionValue == noneOption){
			$(this).show();
			$("#"+childSelectCFID+" option[value='"+optionValue+"']").show();
		}
		else if(optionsToShow.includes(optionValue)){
			$(this).show();
			$("#"+childSelectCFID+" option[value='"+optionValue+"']").show();
		}else{									
			$(this).hide();
			$("#"+childSelectCFID+" option[value='"+optionValue+"']").hide();
		}
	});
	
	if (!optionsToShow.includes($("#"+childSelectCFID).val())){
		$("#"+childSelectCFID).val("-1");
	}
}

function getOptionMappings(pKey, custId, optionid, baseUrl){
	var JSONData;
	AJS.$.ajax({
		url: baseUrl+"/rest/asplselect/1/mappings/"+pKey+"/"+custId+"/"+optionid,
	    type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(data) { JSONData = data; }
	});
	return JSONData;
}
function getProjectKeyByPid(pId, baseUrl){
	var JSONData;
	AJS.$.ajax({
		url: baseUrl+"/rest/api/2/project/"+pId,
	    type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(data) { JSONData = data.key; }
	});
	return JSONData;
}
function getFieldMappings(pKey, baseUrl){
	var JSONData;
	AJS.$.ajax({
		url: baseUrl+"/rest/asplselect/1/mappings/"+pKey,
	    type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(data) { JSONData = data; }
	});
	return JSONData;
}

function disableInlineEdit(parentField, childField) {
	AJS.$(parentField).removeAttr('class');
	AJS.$(parentField).children('.icon').remove();
	AJS.$(parentField).removeAttr( "title" );
	AJS.$(parentField).find('span.overlay-icon').hide();
	
	AJS.$(childField).removeAttr('class');
    AJS.$(childField).children('.icon').remove();
	AJS.$(childField).removeAttr( "title" );
	AJS.$(childField).find('span.overlay-icon').hide();
}