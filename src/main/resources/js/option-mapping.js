var optionMappingHelper = {
    isParentOptionMapped : function (fieldMappingId,parentOptionId) {
        var baseUrl = AJS.params.baseURL;
        jQuery.ajax({
            url:  baseUrl+"/rest/asplselect/1/optionmappings/check/"+fieldMappingId+"/"+parentOptionId,
            type: 'get',
            dataType: 'json',
            async: false,
            success: function (data) {
                $('#option-mapping-form').trigger('submit');
            },
            error: function (err) {
                if(err.status === 409){
                    showError(err.responseText);
                }
            }
        })
    }
}

AJS.$(document).ready(function(){

    $('#child-option-select').select2();

    $(".edit-link").on('click', function (e) {
        e.preventDefault();

        var optionMapData=$(this).attr("data")
        var optionMap = jQuery.parseJSON(optionMapData);

        //Updating fields on form
        $('#h_p_key').val(optionMap.project_key);
        $('#h_m_id').val(optionMap.mapping_id);
        $('#h_f_m_id').val(optionMap.field_mapping_id);
        $('#parent_option_span').text(optionMap.parent_option_name);

        var parent_option_id=optionMap.parent_option_id;
        var child_option_ids=optionMap.child_option_ids;

        child_option_ids=child_option_ids.split(",");
        $('#child-option-select').val(child_option_ids);
        $('#child-option-select').trigger('change');

        AJS.dialog2("#demo-dialog").show();
    });

    $('#option-update-submit-button').on( "click", function() {
        var proceed=confirm("Are you sure you want to update options mapped ?");
        if(proceed)
            return true;
        return false;
    });

    $('#option-add-submit-button').on( "click", function(e) {
        //e.preventDefault();
        var fieldMappingId = $("#mapId").val();
        var parentOptionId = $("#pField").val();
        var childOpts = $("#multiselect").val();
        if (!childOpts) {
            showError("Child field option not selected!!!");
            return false;
        }
        optionMappingHelper.isParentOptionMapped(fieldMappingId, parentOptionId);

        e.preventDefault();
    });

    $(".delete-option-link").on( "click", function() {
        var proceed=confirm("Are you sure you want to delete option mapping ?");
        if(proceed)
            return true;
        return false;
    });

})

var showSuccess = function(message){
    $("#div-option-mapping-error").children("p").text(message);
    $("#div-option-mapping-error").removeClass("hidden").removeClass("aui-message-error");
    $("#div-option-mapping-error").addClass("aui-message-success");
    $("#div-option-mapping-error").show().delay(2000).fadeOut();
}

var showError = function(message){
    $("#div-option-mapping-error").children("p").text(message);
    $("#div-option-mapping-error").removeClass("hidden").removeClass("aui-message-success");
    $("#div-option-mapping-error").addClass("aui-message-error");
    $("#div-option-mapping-error").show().delay(2000).fadeOut();
}