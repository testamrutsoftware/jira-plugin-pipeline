package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.model.ProjFieldMappingModel;

public interface ProjectFieldOptionMappingService {
    ProjFieldMappingModel getMappingData(String projectKey);
    ProjFieldMappingModel getMappingData(String projectKey, String parentFieldId, String parentOptionId);
}

