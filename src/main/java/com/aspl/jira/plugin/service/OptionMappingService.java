package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.ao.entity.Mapping;

import java.util.List;

public interface OptionMappingService {
    List<Mapping> getAllOptionMappingList(int fieldMappingId);
    void addOptionMapping(String parentOptionId, String[] childOptionIds,int fieldMappingId);
    void updateOptionMapping(String projectKey, int fieldMappingId, int optionMappingId, String[] childOptions);
    void deleteOptionMapping(int fieldMappingId, int optionMappingId);
    String getProjectByFieldMappingById(int fieldMappingId);
    boolean checkOptionAlreadyMapped(int fieldMappingId, String parentOptionId);
    Mapping[] getOptionMappingByPKeyFieldMappingID(String pKey, int fieldMappingId);
    Mapping[] getOptionMappingByPKeyFieldMappingPOptionID(String pKey, int fieldMappingId, String parentOptionId);
    void deleteAllOptionMapping(int fieldMappingId);
    void updateOptionMappingProjectKey(int fieldMappingId, String project);
}
