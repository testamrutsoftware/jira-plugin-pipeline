package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.model.FieldOptions;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;

import java.util.List;
import java.util.Map;

public interface JiraService {
    List<Project> getAllProjectList();
    List<CustomField> getCustomFieldList();
    boolean isCustomFieldDeleted(String customFieldId);
    CustomFieldManager getCustomFieldManager();
    FieldOptions getCustomFieldOptions(String customFieldID);
    OptionsManager getOptionManager();
    Map<String,String> getOptionIDWithNameMap(String... cFieldIDs);
    String getJiraBaseURL();
}
