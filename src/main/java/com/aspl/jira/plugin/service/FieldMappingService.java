package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.ao.entity.Fields;

import java.util.List;

public interface FieldMappingService {
    boolean isProjecPFieldCFieldMapped(String project, String pField, String cField);
    boolean isProjecCFieldPFieldMapped(String project, String cField, String pField);
    void addFieldMapping(String pField, String cField, String project);
    List<Fields> getAllFieldMappingList();
    void deleteFieldMapping(String project, int fieldMappingId);
    void updateFieldMapping(String project, int fieldMappingId);
    String getParentFieldId(int fieldMapingId);
    String getChildFieldId(int fieldMapingId);
    Fields getFieldMappingById(int fieldMappingId);
    Fields[] getFieldMappingsByPKey(String projectKey);
    Fields[] getFieldMappingsByPKeyPField(String projectKey, String pField);
}
