package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.ao.service.FieldMappingAO;
import com.aspl.jira.plugin.ao.entity.Fields;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Scanned
@Named("fieldMappingService")
public class FieldMappingServiceImpl implements FieldMappingService{

    @Inject
    private FieldMappingAO fieldMappingAO;

    @Inject
    private JiraService jiraService;

    @Inject
    OptionMappingService optionMappingService;

    private static final Logger log = LoggerFactory.getLogger(FieldMappingServiceImpl.class);

    public boolean isProjecPFieldCFieldMapped(String project, String pField, String cField){
        if(fieldMappingAO.getData(project,pField,cField).length != 0)
            return true;
        else
            return false;
    }

    public boolean isProjecCFieldPFieldMapped(String project, String cField, String pField){
        if(fieldMappingAO.getData(project,cField,pField).length != 0)
            return true;
        else
            return false;
    }

    public void addFieldMapping(String pField, String cField, String project){
        fieldMappingAO.add(pField, cField, project);
        log.debug(String.format("Field Mapping added  pField : %s, cField : %s, project : %s", pField, cField, project));
    }

    public void deleteFieldMapping(String project, int fieldMappingId){
        log.debug(String.format("Delete Field Mapping project : %s, fieldMappingId : %s",project,fieldMappingId));
        fieldMappingAO.delete(project, fieldMappingId);
        optionMappingService.deleteAllOptionMapping(fieldMappingId);
    }

    @Override
    public void updateFieldMapping(String project, int fieldMappingId) {
        fieldMappingAO.updateFieldMappingProject(project,fieldMappingId);
        optionMappingService.updateOptionMappingProjectKey(fieldMappingId,project);
        log.debug(String.format("Field Mapping updated  project : %s, fieldMappingId : %s", project,fieldMappingId));
    }

    public List<Fields> getAllFieldMappingList(){
        Fields[]  fieldMapping = fieldMappingAO.getAllFieldMappingData();
        List<Fields> fieldMappings=new ArrayList<>();
        for(Fields mapping : fieldMapping)
        {
            if(jiraService.isCustomFieldDeleted(mapping.getParentField())||jiraService.isCustomFieldDeleted(mapping.getChildField())) {
                log.debug(String.format("One of the custom field from %s, %s is deleted", mapping.getParentField(),mapping.getChildField()));
                deleteFieldMapping(mapping.getProjectKey(), mapping.getID());
                log.debug(String.format("Deleted Field Mapping : %s",mapping.getID()));
            }
            else
                fieldMappings.add(mapping);
        }
        return fieldMappings;
    }

    public String getParentFieldId(int fieldMapingId){
       return fieldMappingAO.getParentFieldID(fieldMapingId);
    }

    public String getChildFieldId(int fieldMapingId){
        return fieldMappingAO.getChildFieldID(fieldMapingId);
    }

    @Override
    public Fields getFieldMappingById(int fieldMappingId) {
        return fieldMappingAO.getFieldMappingById(fieldMappingId);
    }

    @Override
    public Fields[] getFieldMappingsByPKey(String projectKey) {
        return fieldMappingAO.getFieldMappingsByPKey(projectKey);
    }

    @Override
    public Fields[] getFieldMappingsByPKeyPField(String projectKey, String pField) {
        return fieldMappingAO.getFieldMappingsByPKeyPField(projectKey,pField);
    }

    //Injected Services
    public FieldMappingAO getFieldMappingAO() {
        return fieldMappingAO;
    }
    public void setFieldMappingAO(FieldMappingAO fieldMappingAO) {
        this.fieldMappingAO = fieldMappingAO;
    }
    public JiraService getJiraService() {
        return jiraService;
    }
    public void setJiraService(JiraService jiraService) {
        this.jiraService = jiraService;
    }
    public OptionMappingService getOptionMappingService() {
        return optionMappingService;
    }
    public void setOptionMappingService(OptionMappingService optionMappingService) {
        this.optionMappingService = optionMappingService;
    }
}
