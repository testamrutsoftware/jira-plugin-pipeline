package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.ao.service.OptionMappingAO;
import com.aspl.jira.plugin.ao.entity.Mapping;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Scanned
@Named("optionMappingService")
public class OptionMappingServiceImpl implements OptionMappingService{

    @Inject
    private OptionMappingAO optionMappingAO;

    @Inject
    private JiraService jiraService;

    @Inject
    FieldMappingService fieldMappingService;

    private static final Logger log = LoggerFactory.getLogger(OptionMappingServiceImpl.class);

    @Override
    public List<Mapping> getAllOptionMappingList(int fieldMappingId) {
        Mapping[] optionMapping = optionMappingAO.getOptionMappingByFieldMappingID(fieldMappingId);
        return Arrays.asList(optionMapping);
    }

    @Override
    public void addOptionMapping(String pOptionId, String[] cOptionIds, int fieldMappingId) {

        Map<String, String> pOptionIdWithNameMap = jiraService.getOptionIDWithNameMap(pOptionId);
        pOptionId=pOptionIdWithNameMap.get("optionIDs");
        String pOptionName=pOptionIdWithNameMap.get("optionNames");

        Map<String, String> cOptionIdWithNameMap = jiraService.getOptionIDWithNameMap(cOptionIds);
        String cOptionId=cOptionIdWithNameMap.get("optionIDs");
        String cOptionNames=cOptionIdWithNameMap.get("optionNames");

        String projectKey = fieldMappingService.getFieldMappingById(fieldMappingId).getProjectKey();

        optionMappingAO.add(pOptionId, pOptionName, cOptionId, cOptionNames, fieldMappingId, projectKey);
        log.debug(String.format("Option Mapping added  pOptionId : %s, pOptionName : %s, cOptionId : %s, cOptionNames : %s, fieldMappingId : %s, projectKey : %s", pOptionId, pOptionName, cOptionId, cOptionNames, fieldMappingId, projectKey));
    }

    @Override
    public void updateOptionMapping(String projectKey, int fieldMappingId, int optionMappingId, String[] childOptions) {
        if(childOptions==null)
        {
            Mapping mapping = optionMappingAO.isAlreadyMapped(projectKey, fieldMappingId, optionMappingId);
            if(mapping!=null)
            {
                log.debug(String.format("Found child options empty. Deleting option mapping "+mapping));
                optionMappingAO.delete(mapping);
            }
            return;
        }


        Map<String, String> optionIdWithNameMap = jiraService.getOptionIDWithNameMap(childOptions);
        String cFieldOptions=optionIdWithNameMap.get("optionIDs");
        String cFieldOptionsName=optionIdWithNameMap.get("optionNames");

        Mapping mapping = optionMappingAO.isAlreadyMapped(projectKey, fieldMappingId, optionMappingId);
        if(mapping!=null){
            optionMappingAO.update(cFieldOptions,cFieldOptionsName,String.valueOf(fieldMappingId),projectKey,mapping);
            log.debug("Updated option mapping with "+cFieldOptionsName);
        }
        else{
            log.debug("Option mapping not found to update");
        }

    }

    @Override
    public void deleteOptionMapping(int fieldMappingId, int optionMappingId) {
        optionMappingAO.delete(fieldMappingId, optionMappingId);
        log.debug(String.format("Option Mapping deleted  fieldMappingId : %s, optionMappingId : %s", fieldMappingId, optionMappingId));
    }

    @Override
    public String getProjectByFieldMappingById(int fieldMappingId) {
        return fieldMappingService.getFieldMappingById(fieldMappingId).getProjectKey();
    }

    @Override
    public boolean checkOptionAlreadyMapped(int fieldMappingId, String parentOptionId) {
        return optionMappingAO.checkOptionAlreadyMapped(fieldMappingId,parentOptionId);
    }

    @Override
    public Mapping[] getOptionMappingByPKeyFieldMappingID(String pKey, int fieldMappingId) {
        return optionMappingAO.getOptionMappingByPKeyFieldMappingID(pKey,fieldMappingId);
    }

    @Override
    public Mapping[] getOptionMappingByPKeyFieldMappingPOptionID(String pKey, int fieldMappingId, String parentOptionId) {
        return optionMappingAO.getOptionMappingByPKeyFieldMappingPOptionID(pKey, fieldMappingId, parentOptionId);
    }

    @Override
    public void deleteAllOptionMapping(int fieldMappingId) {
        optionMappingAO.deleteAllOptionMapping(fieldMappingId);
        log.debug(String.format("All Option mapping deleted  fieldMappingId : %s", fieldMappingId));
    }

    @Override
    public void updateOptionMappingProjectKey(int fieldMappingId, String project) {
        optionMappingAO.updateOptionMappingProjectKey(fieldMappingId, project);
    }

    //Injected Services
    public OptionMappingAO getOptionMappingAO() {
        return optionMappingAO;
    }
    public void setOptionMappingAO(OptionMappingAO optionMappingAO) {
        this.optionMappingAO = optionMappingAO;
    }
    public JiraService getJiraService() {
        return jiraService;
    }
    public void setJiraService(JiraService jiraService) {
        this.jiraService = jiraService;
    }
    public FieldMappingService getFieldMappingService() {
        return fieldMappingService;
    }
    public void setFieldMappingService(FieldMappingService fieldMappingService) {
        this.fieldMappingService = fieldMappingService;
    }
}
