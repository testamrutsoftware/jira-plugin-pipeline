package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.ao.entity.Fields;
import com.aspl.jira.plugin.ao.entity.Mapping;
import com.aspl.jira.plugin.model.ProjFieldMappingModel;

import javax.inject.Inject;
import javax.inject.Named;
import  java.util.List;
import java.util.ArrayList;

import com.aspl.jira.plugin.model.FieldMappingModel;
import com.aspl.jira.plugin.model.OptionMappingsModel;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;

@Scanned
@Named("projectFieldOptionMappingService")
public class ProjectFieldOptionMappingServiceImpl implements ProjectFieldOptionMappingService{

    @Inject
    private FieldMappingService fieldMappingService;

    @Inject
    OptionMappingService optionMappingService;

    @Override
    public ProjFieldMappingModel getMappingData(String projectKey) {
        List<FieldMappingModel> fieldMappingList = new ArrayList();
        Fields[] fieldMappings = fieldMappingService.getFieldMappingsByPKey(projectKey);
        for(Fields fieldMapping : fieldMappings){
            List<OptionMappingsModel> optionMappingsList = new ArrayList<OptionMappingsModel>();
            Mapping[] optionMappings = optionMappingService.getOptionMappingByPKeyFieldMappingID(projectKey, fieldMapping.getID());
            for(Mapping optionMapping : optionMappings){
                optionMappingsList.add(new OptionMappingsModel(optionMapping.getID(), optionMapping.getFieldMappingId(), optionMapping.getProjectKey(), optionMapping.getParentOption(), optionMapping.getParentOptionName(), optionMapping.getChildOptions(), optionMapping.getChildOptionsName()));
            }
            fieldMappingList.add(new FieldMappingModel(fieldMapping.getID(), fieldMapping.getProjectKey(), fieldMapping.getParentField(), fieldMapping.getChildField(), optionMappingsList));
        }
        return new ProjFieldMappingModel(projectKey, fieldMappingList);
    }

    @Override
    public ProjFieldMappingModel getMappingData(String projectKey, String parentFieldId, String parentOptionId) {
        List<FieldMappingModel> fieldMappingList = new ArrayList();
        Fields[] fieldMappings = fieldMappingService.getFieldMappingsByPKeyPField(projectKey, parentFieldId);
        for(Fields fieldMapping : fieldMappings) {
            List<OptionMappingsModel> optionMappingsList = new ArrayList<OptionMappingsModel>();
            Mapping[] optionMappings = optionMappingService.getOptionMappingByPKeyFieldMappingPOptionID(projectKey, fieldMapping.getID(), parentOptionId);
            for(Mapping optionMapping : optionMappings){
                optionMappingsList.add(new OptionMappingsModel(optionMapping.getID(), optionMapping.getFieldMappingId(), optionMapping.getProjectKey(), optionMapping.getParentOption(), optionMapping.getParentOptionName(), optionMapping.getChildOptions(), optionMapping.getChildOptionsName()));
            }
            fieldMappingList.add(new FieldMappingModel(fieldMapping.getID(), fieldMapping.getProjectKey(), fieldMapping.getParentField(), fieldMapping.getChildField(), optionMappingsList));
        }
        return  new ProjFieldMappingModel(projectKey, fieldMappingList);
    }

    //Injected services
    public FieldMappingService getFieldMappingService() {
        return fieldMappingService;
    }
    public void setFieldMappingService(FieldMappingService fieldMappingService) {
        this.fieldMappingService = fieldMappingService;
    }
    public OptionMappingService getOptionMappingService() {
        return optionMappingService;
    }
    public void setOptionMappingService(OptionMappingService optionMappingService) {
        this.optionMappingService = optionMappingService;
    }
}
