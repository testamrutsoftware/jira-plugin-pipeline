package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.model.FieldOptions;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named("jiraService")
public class JiraServiceImpl implements JiraService{

    private FieldOptions fieldOptions;
    private String customFieldName;

    @Override
    public List<Project> getAllProjectList(){
        List<Project> allProjects = ComponentAccessor.getProjectManager().getProjectObjects();
        return allProjects;
    }

    @Override
    public List<CustomField> getCustomFieldList(){
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        List<CustomField> customFields = customFieldManager.getCustomFieldObjects();
        List<CustomField> customFieldsList = new ArrayList<CustomField>();
        for (CustomField customField : customFields) {
            if(customField.getCustomFieldType().getKey().equalsIgnoreCase("com.atlassian.jira.plugin.system.customfieldtypes:select")) {
                   customFieldsList.add(customField);
             }
        }
        return customFieldsList;
    }

    @Override
    public boolean isCustomFieldDeleted(String customFieldId){
        CustomField customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(customFieldId);
        if(customField!=null)
            return false;
        else
            return true;
    }

    @Override
    public CustomFieldManager getCustomFieldManager() {
        return ComponentAccessor.getCustomFieldManager();
    }

    @Override
    public FieldOptions getCustomFieldOptions(String customFieldID) {
        fieldOptions=new FieldOptions();
        customFieldName = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(customFieldID).getName();
        Options options=ComponentAccessor.getOptionsManager().getOptions(ComponentAccessor.getCustomFieldManager().getCustomFieldObject(customFieldID).getConfigurationSchemes().listIterator().next().getOneAndOnlyConfig());
        fieldOptions.setCustomFieldID(customFieldID);
        fieldOptions.setCustomFieldName(customFieldName);
        fieldOptions.setOptions(options);
        return fieldOptions;
    }

    @Override
    public OptionsManager getOptionManager() {
        return ComponentAccessor.getOptionsManager();
    }

    @Override
    public Map<String, String> getOptionIDWithNameMap(String... cFieldIDs) {
        String cFieldOptions = "", cFieldOptionsName = "";
        int i = 0;
        for(String option : cFieldIDs) {
            if(i == 0) {
                cFieldOptions = cFieldOptions + option;
                cFieldOptionsName = cFieldOptionsName + ComponentAccessor.getOptionsManager().findByOptionId(Long.parseLong(option)).getValue();
                i++;
            }else {
                cFieldOptions = cFieldOptions + "," + option;
                cFieldOptionsName = cFieldOptionsName + ", " + ComponentAccessor.getOptionsManager().findByOptionId(Long.parseLong(option)).getValue();
            }
        }
        Map<String,String> optionIDWithNameMap = new HashMap();
        optionIDWithNameMap.put("optionIDs",cFieldOptions);
        optionIDWithNameMap.put("optionNames",cFieldOptionsName);

        return optionIDWithNameMap;
    }

    @Override
    public String getJiraBaseURL() {
        return ComponentAccessor.getApplicationProperties().getString("jira.baseurl");
    }

}
