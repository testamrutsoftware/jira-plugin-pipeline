package com.aspl.jira.plugin.action;

import com.aspl.jira.plugin.ao.entity.Mapping;
import com.aspl.jira.plugin.model.FieldOptions;
import com.aspl.jira.plugin.service.FieldMappingService;
import com.aspl.jira.plugin.service.JiraService;
import com.aspl.jira.plugin.service.OptionMappingService;
import com.aspl.jira.plugin.util.AtlassianLicenseChecker;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.google.gson.Gson;

import javax.inject.Inject;
import java.util.List;

@Scanned
public class OptionMappingAction  extends JiraWebActionSupport {

    @Inject
    private JiraService jiraService;

    @Inject
    FieldMappingService fieldMappingService;

    @Inject
    OptionMappingService optionMappingService;

    @Inject
    private AtlassianLicenseChecker licenseChecker;

    private String projectKey;
    private int fieldMappingId;
    private int optionMappingId;
    private FieldOptions parentField;
    private FieldOptions childField;
    private List<Mapping> optionMappingList;
    private String pField;
    private String[] cField;
    private String baseURL;

    private Gson jsonHelper;

    private OptionsManager optionsManager;

    public String doDefault() throws Exception {
        try {
            doDefaultValidation();
            if(hasAnyErrors()) {
                return "error";
            }
            return "input"; //returns SUCCESS
        }catch (Exception e) {
            log.error(e);
            addErrorMessage(getText("err.something.wrong"));
            return "error";
        }
    }

    public void doDefaultValidation() {
        if(fieldMappingService.getFieldMappingById(getFieldMappingId())==null)
            addErrorMessage(getText("err.mapping.not.found"));
    }


    public void doAddValidation() {
        if(!licenseChecker.validateLicense()){
            addErrorMessage(getText("err.license.failed"));
            return;
        }
        if(getpField()==null || getcField()==null){
            addError("pField","Parent Field can not be empty!!!");
            addError("cField","Child Field can not be empty!!!");
        }
        if(optionMappingService.checkOptionAlreadyMapped(fieldMappingId,getpField())){
            addErrorMessage("Option mapping already added. Please update mapping instead !!!!");
        }
    }

    public void doUpdateValidation() {
        if(!licenseChecker.validateLicense()){
            addErrorMessage(getText("err.license.failed"));
            return;
        }
    }

    public String doAddOptionMapping(){
        try {
            doAddValidation();
            if(hasAnyErrors()) {
                return "input";
            }
            optionMappingService.addOptionMapping(getpField(), getcField(),getFieldMappingId());
            return getRedirect(getBaseURL()+"/OptionMappingAction!default.jspa?fieldMappingId="+getFieldMappingId());
        }catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            addErrorMessage(getText("err.something.wrong"));
            return "error";
        }
    }

    public String doUpdateOptionMapping(){
        try {
            doUpdateValidation();
            if(hasAnyErrors()) {
                setFieldMappingId(getFieldMappingId());
                return "input";
            }
            optionMappingService.updateOptionMapping(getProjectKey(),getFieldMappingId(),getOptionMappingId(),getcField());
            return getRedirect(getBaseURL()+"/OptionMappingAction!default.jspa?fieldMappingId="+getFieldMappingId());
        }catch (Exception e) {
            log.error(e);
            addErrorMessage(getText("err.something.wrong"));
            return "error";
        }
    }

    public String doDeleteOptionMapping(){
        try {
            if(!licenseChecker.validateLicense()){
                addErrorMessage(getText("err.license.failed"));
                return "input";
            }
            optionMappingService.deleteOptionMapping(getFieldMappingId(),getOptionMappingId());
            return getRedirect(getBaseURL()+"/OptionMappingAction!default.jspa?fieldMappingId="+getFieldMappingId());
        }catch (Exception e) {
            log.error(e);
            addErrorMessage(getText("err.something.wrong"));
            return "error";
        }
    }

    //Injected components
    public JiraService getJiraService() {
        return jiraService;
    }
    public void setJiraService(JiraService jiraService) {
        this.jiraService = jiraService;
    }
    public FieldMappingService getFieldMappingService() {
        return fieldMappingService;
    }
    public void setFieldMappingService(FieldMappingService fieldMappingService) {
        this.fieldMappingService = fieldMappingService;
    }
    public OptionMappingService getOptionMappingService() {
        return optionMappingService;
    }
    public void setOptionMappingService(OptionMappingService optionMappingService) {
        this.optionMappingService = optionMappingService;
    }
    public AtlassianLicenseChecker getLicenseChecker() {
        return licenseChecker;
    }
    public void setLicenseChecker(AtlassianLicenseChecker licenseChecker) {
        this.licenseChecker = licenseChecker;
    }


    //Form Fields/request parameters
    public int getFieldMappingId() {
        return fieldMappingId;
    }
    public void setFieldMappingId(int fieldMappingId) {
        this.fieldMappingId = fieldMappingId;
    }
    public String getProjectKey() {
        return optionMappingService.getProjectByFieldMappingById(getFieldMappingId());
    }
    public FieldOptions getParentField() {
        return jiraService.getCustomFieldOptions(fieldMappingService.getParentFieldId(getFieldMappingId()));
    }
    public FieldOptions getChildField() {
        return jiraService.getCustomFieldOptions(fieldMappingService.getChildFieldId(getFieldMappingId()));
    }
    public List<Mapping> getOptionMappingList() {
        return optionMappingService.getAllOptionMappingList(getFieldMappingId());
    }
    public OptionsManager getOptionsManager() {
        return jiraService.getOptionManager();
    }
    public String getpField() {
        return pField;
    }
    public void setpField(String pField) {
        this.pField = pField;
    }
    public String[] getcField() {
        return cField;
    }
    public void setcField(String[] cField) {
        this.cField = cField;
    }
    public int getOptionMappingId() {
        return optionMappingId;
    }
    public void setOptionMappingId(int optionMappingId) {
        this.optionMappingId = optionMappingId;
    }
    public Gson getJsonHelper() {
        return new Gson();
    }
    public String getBaseURL() {
        return jiraService.getJiraBaseURL()+"/secure";
    }
}
