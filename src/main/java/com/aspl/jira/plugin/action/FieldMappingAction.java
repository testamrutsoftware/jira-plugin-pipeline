package com.aspl.jira.plugin.action;

import com.aspl.jira.plugin.ao.entity.Fields;
import com.aspl.jira.plugin.service.FieldMappingService;
import com.aspl.jira.plugin.service.JiraService;
import com.aspl.jira.plugin.util.AtlassianLicenseChecker;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.google.gson.Gson;

import javax.inject.Inject;
import java.util.List;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;

@Scanned
public class FieldMappingAction  extends JiraWebActionSupport {

    @Inject
    private JiraService jiraService;

    @Inject
    private FieldMappingService fieldMappingService;

    @Inject
    private AtlassianLicenseChecker licenseChecker;

    private List<Project> allProjects;
    private List<CustomField> customFields;
    private List<Fields> fieldMappingList;
    private CustomFieldManager customFieldManager;
    private Gson jsonHelper;

    private String project;
    private String pField;
    private String cField;
    private int fieldMappingId;
    private String baseURL;

    public String doDefault() throws Exception {
        return "input"; //returns SUCCESS
    }

    public void doAddValidation() {
        if(!licenseChecker.validateLicense()){
            addErrorMessage(getText("err.license.failed"));
            return;
        }
        if(getpField()==null || getcField()==null){
            addError("pField",getText("err.pfield.not_empty"));
            addError("cField",getText("err.cfield.not_empty"));
        }
        else if(getpField().equals(getcField())){
           addError("pField",getText("err.p_c_field.not_same"));
           addError("cField",getText("err.c_f_field.not_same"));
        }
        if(fieldMappingService.isProjecPFieldCFieldMapped(getProject(),getpField(),getcField())){
            addErrorMessage(getText("err.mapping.exists"));
        }
        else if(fieldMappingService.isProjecCFieldPFieldMapped(getProject(),getcField(),getpField())){
            addErrorMessage(getText("err.rev.exists"));
        }
    }

    public void doUpdateValidation() {
        if(!licenseChecker.validateLicense()){
            addErrorMessage(getText("err.license.failed"));
            return;
        }
        if(fieldMappingService.isProjecPFieldCFieldMapped(getProject(),getpField(),getcField())){
            addErrorMessage(getText("err.mapping.exists"));
        }
        else if(fieldMappingService.isProjecCFieldPFieldMapped(getProject(),getcField(),getpField())){
            addErrorMessage(getText("err.rev.exists"));
        }
    }

    @com.atlassian.jira.security.xsrf.RequiresXsrfCheck
    public String doAddFieldMapping(){
        try {
            doAddValidation();
            if (hasAnyErrors()) {
               return "input";
            }
            fieldMappingService.addFieldMapping(getpField(), getcField(), getProject());
            return getRedirect(getBaseURL() + "/FieldMappingAction!default.jspa");
        }catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            addErrorMessage(getText("err.something.wrong"));
            return "error";
        }
    }

    public String doDeleteFieldMapping(){
        try {
            if(!licenseChecker.validateLicense()){
                addErrorMessage(getText("err.license.failed"));
                return "input";
            }
            fieldMappingService.deleteFieldMapping(getProject(), getFieldMappingId());
            return getRedirect(getBaseURL() + "/FieldMappingAction!default.jspa");
        }
        catch (Exception e) {
            log.error(e);
            addErrorMessage(getText("err.something.wrong"));
            return "error";
        }
    }

    public String doUpdateFieldMapping(){
        try {
            doUpdateValidation();
            if (hasAnyErrors()) {
                return "input";
            }
            fieldMappingService.updateFieldMapping(getProject(), getFieldMappingId());
            return getRedirect(getBaseURL() + "/FieldMappingAction!default.jspa");
        } catch (Exception e){
            log.error(e);
            addErrorMessage(getText("err.something.wrong"));
            return "error";
        }
    }

    //default data on page
    public List<Project> getAllProjects() {
       return  jiraService.getAllProjectList();
    }
    public List<CustomField> getCustomFields() {
        return jiraService.getCustomFieldList();
    }
    public List<Fields> getFieldMappingList() {
        return fieldMappingService.getAllFieldMappingList();
    }
    public CustomFieldManager getCustomFieldManager() {
        return jiraService.getCustomFieldManager();
    }
    public Gson getJsonHelper() {
        return new Gson();
    }

    //Injected components
    public JiraService getJiraService() {
        return jiraService;
    }
    public void setJiraService(JiraService jiraService) {
        this.jiraService = jiraService;
    }
    public FieldMappingService getFieldMappingService() {
        return fieldMappingService;
    }
    public void setFieldMappingService(FieldMappingService fieldMappingService) {
        this.fieldMappingService = fieldMappingService;
    }
    public AtlassianLicenseChecker getLicenseChecker() {
        return licenseChecker;
    }
    public void setLicenseChecker(AtlassianLicenseChecker licenseChecker) {
        this.licenseChecker = licenseChecker;
    }

    //Form Fields
    public String getProject() {
        return project;
    }
    public void setProject(String project) {
        this.project = project;
    }
    public String getpField() {
        return pField;
    }
    public void setpField(String pField) {
        this.pField = pField;
    }
    public String getcField() {
        return cField;
    }
    public void setcField(String cField) {
        this.cField = cField;
    }
    public int getFieldMappingId() {
        return fieldMappingId;
    }
    public void setFieldMappingId(int fieldMappingId) {
        this.fieldMappingId = fieldMappingId;
    }
    public String getBaseURL() {
        return jiraService.getJiraBaseURL()+"/secure";
    }
}
