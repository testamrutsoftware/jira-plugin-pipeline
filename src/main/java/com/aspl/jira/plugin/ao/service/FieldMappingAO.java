package com.aspl.jira.plugin.ao.service;

import com.aspl.jira.plugin.ao.entity.Fields;
import com.atlassian.activeobjects.tx.Transactional;

@Transactional
public interface FieldMappingAO {
    Fields add(String parentField, String childField, String pKey);
    Fields[] getData(String project, String pField, String cField);
    Fields[] getAllFieldMappingData();
    void delete(String pKey, int fieldMappingId);
    Fields getFieldMappingByProAndId(String pKey, int fieldMappingId);
    Fields getFieldMappingById(int fieldMappingId);
    void updateFieldMappingProject(String projectKey, int fieldMappingId );
    String getParentFieldID(int fieldMappingId);
    String getChildFieldID(int fieldMappingId);
    Fields[] getFieldMappingsByPKey(String projectKey);
    Fields[] getFieldMappingsByPKeyPField(String projectKey, String pField);

}
