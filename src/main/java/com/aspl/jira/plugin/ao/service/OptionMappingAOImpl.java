package com.aspl.jira.plugin.ao.service;

import com.aspl.jira.plugin.ao.entity.Mapping;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;

@Named("optionMappingAO")
public class OptionMappingAOImpl implements OptionMappingAO{

    @ComponentImport
    @Inject
    private ActiveObjects ao;

    @Override
    public void add(String parentFieldOption, String parentFieldOptionName, String childFieldOptions, String childFieldOptionsName, int mapId, String pKey) {
        final Mapping mapping = ao.create(Mapping.class);
        mapping.setParentOption(parentFieldOption);
        mapping.setParentOptionName(parentFieldOptionName);
        mapping.setChildOptions(childFieldOptions);
        mapping.setChildOptionsName(childFieldOptionsName);
        mapping.setFieldMappingId(String.valueOf(mapId));
        mapping.setProjectKey(pKey);
        mapping.save();
    }


    @Override
    public Mapping[] getOptionMappingByFieldMappingID(int fieldMappingId) {
        Mapping[]  queryResult = ao.find(Mapping.class, Query.select().where("FIELD_MAPPING_ID = ?", String.valueOf(fieldMappingId)));
        return queryResult;
    }

    @Override
    public Mapping isAlreadyMapped(String pKey, int fieldMappingId, int optionMappingId) {
        Mapping mapping=null;
        Mapping[] mappings=ao.find(Mapping.class, Query.select().where("PROJECT_KEY = ? AND FIELD_MAPPING_ID = ? AND ID = ?", pKey, String.valueOf(fieldMappingId), optionMappingId));
        if(mappings.length>0)
            mapping = mappings[0];
        return mapping;
    }

    public void delete(Mapping mapping) {
        // TODO Auto-generated method stub
        ao.delete(mapping);
    }

    @Override
    public void delete(int fieldMappingId, int optionMappingId) {
        Mapping[] mappings=ao.find(Mapping.class, Query.select().where("FIELD_MAPPING_ID = ? AND ID = ?", String.valueOf(fieldMappingId), optionMappingId));
        for (Mapping optionMapping: mappings) {
            ao.delete(optionMapping);
        }
    }

    @Override
    public boolean checkOptionAlreadyMapped(int fieldMappingId, String parentOptionId) {
        Mapping[] mappings=ao.find(Mapping.class, Query.select().where("FIELD_MAPPING_ID = ? AND PARENT_OPTION = ?", String.valueOf(fieldMappingId), String.valueOf(parentOptionId)));
        if(mappings.length==0)
            return false;
        else
            return true;
    }

    @Override
    public Mapping[] getOptionMappingByPKeyFieldMappingID(String pKey, int fieldMappingId) {
        Mapping[] mappings=ao.find(Mapping.class, Query.select().where("PROJECT_KEY = ? AND FIELD_MAPPING_ID = ?", pKey, String.valueOf(fieldMappingId)));
        return mappings;
    }

    @Override
    public Mapping[] getOptionMappingByPKeyFieldMappingPOptionID(String pKey, int fieldMappingId, String parentOptionId){
        Mapping[] mappings=ao.find(Mapping.class, Query.select().where("PROJECT_KEY = ? AND FIELD_MAPPING_ID = ? AND PARENT_OPTION = ?", pKey, String.valueOf(fieldMappingId), String.valueOf(parentOptionId)));
        return mappings;
    }

    @Override
    public void deleteAllOptionMapping(int fieldMappingId) {
        Mapping[] mappings=ao.find(Mapping.class, Query.select().where("FIELD_MAPPING_ID = ?", String.valueOf(fieldMappingId)));
        for (Mapping optionMapping: mappings) {
            ao.delete(optionMapping);
        }
    }

    @Override
    public void updateOptionMappingProjectKey(int fieldMappingId, String project) {
        Mapping[] mappings=ao.find(Mapping.class, Query.select().where("FIELD_MAPPING_ID = ?", String.valueOf(fieldMappingId)));
        for (Mapping optionMapping: mappings) {
            optionMapping.setProjectKey(project);
            optionMapping.save();
        }
    }

    public Mapping update(String childFieldOptions, String childFieldOptionsName, String fieldMappingId, String pKey, Mapping mappingData) {
        // TODO Auto-generated method stub
        mappingData.setChildOptions(childFieldOptions);
        mappingData.setChildOptionsName(childFieldOptionsName);
        mappingData.setFieldMappingId(fieldMappingId);
        mappingData.setProjectKey(pKey);
        mappingData.save();
        return mappingData;
    }

    //Injected Services
    public ActiveObjects getAo() {
        return ao;
    }
    public void setAo(ActiveObjects ao) {
        this.ao = ao;
    }
}
