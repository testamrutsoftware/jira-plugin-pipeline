package com.aspl.jira.plugin.ao.entity;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.PrimaryKey;

//Field mapping
@Preload
public interface Fields extends Entity
{
    String getParentField();

    void setParentField(String pfield);

    String getChildField();

    void setChildField(String cfield);

    String getProjectKey();
 
    void setProjectKey(String pKey);
    
    @PrimaryKey
    @AutoIncrement
    int getID();
    
}
