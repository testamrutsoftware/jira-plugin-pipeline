package com.aspl.jira.plugin.ao.entity;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.PrimaryKey;
import net.java.ao.schema.StringLength;

//Option mapping
@Preload
public interface Mapping extends Entity
{
    String getParentOption();

    void setParentOption(String pfield);
    
    String getParentOptionName();

    void setParentOptionName(String pfield);
    
    @StringLength(value=StringLength.UNLIMITED)
    String getChildOptions();

    @StringLength(value=StringLength.UNLIMITED)
    void setChildOptions(String cfield);
    
    @StringLength(value=StringLength.UNLIMITED)
    String getChildOptionsName();

    @StringLength(value=StringLength.UNLIMITED)
    void setChildOptionsName(String cfield);

    String getFieldMappingId();
 
    void setFieldMappingId(String id);
    
    String getProjectKey();
    
    void setProjectKey(String pKey);
    
    @PrimaryKey
    @AutoIncrement
    int getID();
}
