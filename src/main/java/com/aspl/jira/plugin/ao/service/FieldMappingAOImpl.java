package com.aspl.jira.plugin.ao.service;

import com.aspl.jira.plugin.ao.entity.Fields;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;

@Named("fieldMappingAO")
public class FieldMappingAOImpl implements FieldMappingAO{

    @ComponentImport
    @Inject
    private ActiveObjects ao;

    private String fieldId;



    @Override
    public Fields add(String parentField, String childField, String pKey){
        // TODO Auto-generated method stub
        final Fields fields = ao.create(Fields.class);
        fields.setParentField(parentField);
        fields.setChildField(childField);
        fields.setProjectKey(pKey);
        fields.save();
        return fields;
    }

    public Fields[] getData(String project, String pField, String cField){
        Fields[]  queryResult = ao.find(Fields.class, Query.select().where("PROJECT_KEY = ? AND PARENT_FIELD = ? AND CHILD_FIELD = ?", project, pField, cField ));
        return queryResult;
    }

    public Fields[] getAllFieldMappingData(){
        Fields[]  queryResult =  ao.find(Fields.class);
        return queryResult;
    }

    @Override
    public void delete(String pKey, int fieldMappingId) {
        Fields fieldMapping=getFieldMappingByProAndId(pKey,fieldMappingId);
        ao.delete(fieldMapping);
    }

    @Override
    public Fields getFieldMappingByProAndId(String pKey, int fieldMappingId) {
        Fields[]  queryResult = ao.find(Fields.class, Query.select().where("PROJECT_KEY = ? AND ID = ?", pKey, fieldMappingId ));
        return queryResult[0];
    }

    @Override
    public Fields getFieldMappingById(int fieldMappingId) {
        Fields fieldMapping=null;
        Fields[]  queryResult = ao.find(Fields.class, Query.select().where("ID = ?", fieldMappingId ));
        if(queryResult.length>0)
            fieldMapping=queryResult[0];
        return fieldMapping;
    }

    @Override
    public void updateFieldMappingProject(String projectKey, int fieldMappingId) {
        Fields eachProjectMapping = getFieldMappingById(fieldMappingId);
        eachProjectMapping.setProjectKey(projectKey);
        eachProjectMapping.save();
    }

    @Override
    public String getParentFieldID(int fieldMappingId) {
        Fields[]  queryResult = ao.find(Fields.class, Query.select().where("ID = ?", fieldMappingId ));
        if(queryResult.length!=0){
            fieldId=queryResult[0].getParentField();
        }
        return fieldId;
    }

    @Override
    public String getChildFieldID(int fieldMappingId) {
        Fields[]  queryResult = ao.find(Fields.class, Query.select().where("ID = ?", fieldMappingId ));
        if(queryResult.length!=0){
            fieldId=queryResult[0].getChildField();
        }
        return fieldId;
    }

    @Override
    public Fields[] getFieldMappingsByPKey(String projectKey) {
        Fields[]  queryResult = ao.find(Fields.class, Query.select().where("PROJECT_KEY = ?", projectKey ));
        return queryResult;
    }

    @Override
    public Fields[] getFieldMappingsByPKeyPField(String projectKey, String pField) {
        Fields[]  queryResult = ao.find(Fields.class, Query.select().where("PROJECT_KEY = ? AND PARENT_FIELD = ?", projectKey, pField ));
        return queryResult;
    }

    //Injected Services
    public ActiveObjects getAo() {
        return ao;
    }
    public void setAo(ActiveObjects ao) {
        this.ao = ao;
    }
}
