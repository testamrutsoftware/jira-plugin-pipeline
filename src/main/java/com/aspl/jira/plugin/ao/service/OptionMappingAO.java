package com.aspl.jira.plugin.ao.service;

import com.aspl.jira.plugin.ao.entity.Mapping;

public interface OptionMappingAO {
    void add(String pField, String pFieldName, String cFieldOptions, String cFieldOptionsName, int fieldMappingId, String pKey);
    Mapping update(String childFieldOptions, String childFieldOptionsName, String fieldMappingId, String pKey, Mapping mappingData);
    Mapping[] getOptionMappingByFieldMappingID(int fieldMappingId);
    Mapping isAlreadyMapped(String pKey, int fieldMappingId, int optionMappingId);
    void delete(Mapping mapping);
    void delete(int fieldMappingId, int optionMappingId) ;
    boolean checkOptionAlreadyMapped(int fieldMappingId, String parentOptionId);
    Mapping[] getOptionMappingByPKeyFieldMappingID(String pKey, int fieldMappingId);
    Mapping[] getOptionMappingByPKeyFieldMappingPOptionID(String pKey, int fieldMappingId, String parentOptionId);
    void deleteAllOptionMapping(int fieldMappingId);
    void updateOptionMappingProjectKey(int fieldMappingId, String project);
}
