package com.aspl.jira.plugin.rest;

import com.aspl.jira.plugin.model.ProjFieldMappingModel;
import com.aspl.jira.plugin.service.FieldMappingService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.aspl.jira.plugin.service.ProjectFieldOptionMappingService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/mappings")
public class FieldMappingResource {
	@Inject
	private ProjectFieldOptionMappingService projectFieldOptionMappingService;

	@Inject
	private FieldMappingService fieldMappingService;

	@GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{pKey}/{pFId}/{pOId}")
    public Response getMessageFromOption(@PathParam("pKey") String pKey, @PathParam("pFId") String parentFieldId, @PathParam("pOId") String parentOptionId){
    	ProjFieldMappingModel data=projectFieldOptionMappingService.getMappingData(pKey,parentFieldId,parentOptionId);
		return Response.ok(data).build();
    }
	
	@GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{key}")
    public Response getMessageFromProjectKey(@PathParam("key") String pKey){
    	ProjFieldMappingModel data=projectFieldOptionMappingService.getMappingData(pKey);
		return Response.ok(data).build();
    }

	@GET
	@AnonymousAllowed
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/check/{pKey}/{pFId}/{cFId}")
	public Response isFieldMappingExist(@PathParam("pKey") String pKey, @PathParam("pFId") String parentFieldId, @PathParam("cFId") String childFieldId){
		boolean isMapped = fieldMappingService.isProjecPFieldCFieldMapped(pKey, parentFieldId, childFieldId);
		boolean isRevMapped = fieldMappingService.isProjecPFieldCFieldMapped(pKey, childFieldId, parentFieldId);
		if(isMapped)
			return Response.serverError().status(Response.Status.CONFLICT).entity("Combination already Exists Please select unique combination!!!").build();
		else if(isRevMapped)
			return Response.serverError().status(Response.Status.CONFLICT).entity("Reverse combination already Exists Please select unique combination!!!").build();
		else
			return Response.ok().build();
	}

	public ProjectFieldOptionMappingService getProjectFieldOptionMappingService() {
		return projectFieldOptionMappingService;
	}

	public void setProjectFieldOptionMappingService(ProjectFieldOptionMappingService projectFieldOptionMappingService) {
		this.projectFieldOptionMappingService = projectFieldOptionMappingService;
	}
}