package com.aspl.jira.plugin.rest;

import com.aspl.jira.plugin.service.OptionMappingService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/optionmappings")
public class OptionMappingResource {

    @Inject
    private OptionMappingService optionMappingService;

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/check/{fieldMappingId}/{parentOptionId}")
    public Response isMappingExist(@PathParam("fieldMappingId") int fieldMappingId, @PathParam("parentOptionId") String parentOptionId){
        boolean isMapped = optionMappingService.checkOptionAlreadyMapped(fieldMappingId, parentOptionId);
        if(isMapped)
            return Response.serverError().status(Response.Status.CONFLICT).entity("Option mapping already added. Please update mapping instead !!!!").build();
        else
            return Response.ok().build();
    }

}
