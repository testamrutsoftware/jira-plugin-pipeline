package com.aspl.jira.plugin.model;

import com.atlassian.jira.issue.customfields.option.Options;

public class FieldOptions {
    private String customFieldID;
    private String customFieldName;
    private Options options;

    public String getCustomFieldID() {
        return customFieldID;
    }

    public void setCustomFieldID(String customFieldID) {
        this.customFieldID = customFieldID;
    }

    public String getCustomFieldName() {
        return customFieldName;
    }

    public void setCustomFieldName(String customFieldName) {
        this.customFieldName = customFieldName;
    }

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }
}
