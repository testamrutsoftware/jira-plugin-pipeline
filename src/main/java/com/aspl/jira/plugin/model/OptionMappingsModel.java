package com.aspl.jira.plugin.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "optionsmappings")
@XmlAccessorType(XmlAccessType.FIELD)
public class OptionMappingsModel {
	
	@XmlElement
	private int ID;
	
	@XmlElement
	private String FIELD_MAPPING_ID;
	
	@XmlElement
	private String PROJECT_KEY;
	
	@XmlElement
	private String PARENT_OPTION_ID;
	
	@XmlElement
	private String PARENT_OPTION_NAME;
	
	@XmlElement
	private String CHILD_OPTIONS_ID;
	
	@XmlElement
	private String CHILD_OPTIONS_NAME;

	public OptionMappingsModel() {
		
	}

	public OptionMappingsModel(int iD, String fIELD_MAPPING_ID, String pROJECT_KEY, String pARENT_OPTION_ID, String pARENT_OPTION_NAME, String cHILD_OPTIONS_ID, String cHILD_OPTIONS_NAME) {
		ID = iD;
		FIELD_MAPPING_ID = fIELD_MAPPING_ID;
		PROJECT_KEY = pROJECT_KEY;
		PARENT_OPTION_ID = pARENT_OPTION_ID;
		PARENT_OPTION_NAME = pARENT_OPTION_NAME;
		CHILD_OPTIONS_ID = cHILD_OPTIONS_ID;
		CHILD_OPTIONS_NAME = cHILD_OPTIONS_NAME;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getFIELD_MAPPING_ID() {
		return FIELD_MAPPING_ID;
	}

	public void setFIELD_MAPPING_ID(String fIELD_MAPPING_ID) {
		FIELD_MAPPING_ID = fIELD_MAPPING_ID;
	}

	public String getPROJECT_KEY() {
		return PROJECT_KEY;
	}

	public void setPROJECT_KEY(String pROJECT_KEY) {
		PROJECT_KEY = pROJECT_KEY;
	}

	public String getPARENT_OPTION_ID() {
		return PARENT_OPTION_ID;
	}

	public void setPARENT_OPTION_ID(String pARENT_OPTION_ID) {
		PARENT_OPTION_ID = pARENT_OPTION_ID;
	}

	public String getPARENT_OPTION_NAME() {
		return PARENT_OPTION_NAME;
	}

	public void setPARENT_OPTION_NAME(String pARENT_OPTION_NAME) {
		PARENT_OPTION_NAME = pARENT_OPTION_NAME;
	}

	public String getCHILD_OPTIONS_ID() {
		return CHILD_OPTIONS_ID;
	}

	public void setCHILD_OPTIONS_ID(String cHILD_OPTIONS_ID) {
		CHILD_OPTIONS_ID = cHILD_OPTIONS_ID;
	}

	public String getCHILD_OPTIONS_NAME() {
		return CHILD_OPTIONS_NAME;
	}

	public void setCHILD_OPTIONS_NAME(String cHILD_OPTIONS_NAME) {
		CHILD_OPTIONS_NAME = cHILD_OPTIONS_NAME;
	}

	@Override
	public String toString() {
		return "GetMappingsModel [ID=" + ID + ", FIELD_MAPPING_ID=" + FIELD_MAPPING_ID + ", PROJECT_KEY=" + PROJECT_KEY
				+ ", PARENT_OPTION_ID=" + PARENT_OPTION_ID + ", PARENT_OPTION_NAME=" + PARENT_OPTION_NAME
				+ ", CHILD_OPTIONS_ID=" + CHILD_OPTIONS_ID + ", CHILD_OPTIONS_NAME=" + CHILD_OPTIONS_NAME + "]";
	}

}
