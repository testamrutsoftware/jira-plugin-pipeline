package com.aspl.jira.plugin.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "projectmappings")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjFieldMappingModel {
	
	@XmlElement(name = "PROJECT_KEY")
    private String project;
	
	@XmlElement(name = "FIELD_MAPPINGS")
    private List<FieldMappingModel> mappings;

	public ProjFieldMappingModel() {
		
	}

	public ProjFieldMappingModel(String project, List<FieldMappingModel> mappings) {
		this.project = project;
		this.mappings = mappings;
	}
	
	public ProjFieldMappingModel(List<FieldMappingModel> mappings) {
		this.mappings = mappings;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public List<FieldMappingModel> getMappings() {
		return mappings;
	}

	public void setMappings(List<FieldMappingModel> mappings) {
		this.mappings = mappings;
	}
}
