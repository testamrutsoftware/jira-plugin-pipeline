package com.aspl.jira.plugin.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fieldmappings")
@XmlAccessorType(XmlAccessType.FIELD)
public class FieldMappingModel {

    @XmlElement(name = "ID")
    private int id;

    @XmlElement(name = "PROJECT_KEY")
    private String projectKey;
    
    @XmlElement(name = "PARENT_FIELD")
    private String parentField;
    
    @XmlElement(name = "CHILD_FIELD")
    private String childField;
    
    @XmlElement(name = "OPTIONS_MAPPINGS")
    private List<OptionMappingsModel> mappings;
    
    public FieldMappingModel() {
    }

	public FieldMappingModel(int id, String projectKey, String parentField, String childField, List<OptionMappingsModel> mappings) {
		this.id = id;
		this.projectKey = projectKey;
		this.parentField = parentField;
		this.childField = childField;
		this.mappings = mappings;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProjectKey() {
		return projectKey;
	}

	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
	}

	public String getParentField() {
		return parentField;
	}

	public void setParentField(String parentField) {
		this.parentField = parentField;
	}

	public String getChildField() {
		return childField;
	}

	public void setChildField(String childField) {
		this.childField = childField;
	}

	public List<OptionMappingsModel> getMappings() {
		return mappings;
	}

	public void setMappings(List<OptionMappingsModel> mappings) {
		this.mappings = mappings;
	}

	@Override
	public String toString() {
		return "GetFieldMappingModel [id=" + id + ", projectKey=" + projectKey + ", parentField=" + parentField
				+ ", childField=" + childField + ", mappings=" + mappings + "]";
	}
	
}