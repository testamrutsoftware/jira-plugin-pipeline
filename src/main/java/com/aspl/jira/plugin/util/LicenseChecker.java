package com.aspl.jira.plugin.util;

public interface LicenseChecker {
    boolean isLicensed();
    boolean validateLicense();
}
