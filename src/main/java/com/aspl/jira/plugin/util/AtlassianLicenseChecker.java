package com.aspl.jira.plugin.util;

import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.upm.api.license.PluginLicenseEventRegistry;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.LicenseError;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.license.event.PluginLicenseChangeEvent;
import com.atlassian.upm.api.license.event.PluginLicenseCheckEvent;
import com.atlassian.upm.api.license.event.PluginLicenseRemovedEvent;
import com.atlassian.upm.api.util.Option;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

import static com.atlassian.upm.api.util.Option.none;
import static com.atlassian.upm.api.util.Option.some;

/**
 * Listens for Atlassian licensing events, and disables this plugin whenever it does not
 * have a valid license.
 */
@Named("licenseChecker")
public class AtlassianLicenseChecker implements LicenseChecker {
    private final Logger LOG = LoggerFactory.getLogger(getClass());

	@ComponentImport
    private final PluginController pluginController;
    @ComponentImport
    private final PluginLicenseManager pluginLicenseManager;

    private final String pluginKey;
    @Inject
    public AtlassianLicenseChecker(PluginController pluginController,
                                   PluginLicenseManager pluginLicenseManager,
                                   @ComponentImport PluginLicenseEventRegistry pluginLicenseEventRegistry) {
        this.pluginController = pluginController;
        this.pluginLicenseManager = pluginLicenseManager;
        this.pluginKey = pluginLicenseManager.getPluginKey();

        pluginLicenseEventRegistry.register(this);
    }

	/**
     * This event is generated when the plugin becomes enabled - either at application
     * startup time, or when the plugin is installed into an already-running application.
     * The plugin may or may not have a license at this point.
     */
    @EventListener
    public void handleEvent(PluginLicenseCheckEvent event) {
        checkLicense(event.getLicense());
    }

    /**
     * This event base class includes all changes to the plugin license other than its
     * complete removal.
     */
    @EventListener
    public void handleEvent(PluginLicenseChangeEvent event) {
        checkLicense(some(event.getLicense()));
    }

    /**
     * This event is generated if an existing license for the plugin is removed.
     */
    @EventListener
    public void handleEvent(PluginLicenseRemovedEvent event) {
        checkLicense(none(PluginLicense.class));
    }

    private final void checkLicense(Option<PluginLicense> maybeLicense) {
    }

    private final boolean isValidLicense(Option<PluginLicense> maybeLicense) {
        for (PluginLicense license: maybeLicense) {
            if (!license.isValid())
            {
                LOG.trace("Invalid license for plugin \"" + pluginKey + "\" (" + license.getError().getOrElse((LicenseError) null) + ")");
                return false;
            }
            LOG.trace("Validated license for plugin '" + pluginKey + "'");
            return true;
        }
        LOG.trace("No license available for plugin '" + pluginKey + "'");
        return false;
    }

    /**
     * Validates the plugin's license. Disables the plugin if the license is invalid.
     * Returns true if the license is valid, false if not.
     * @return true if the license is valid, false if not.
     */
    public boolean validateLicense() {
        Option<PluginLicense> license = pluginLicenseManager.getLicense();
        checkLicense(license);
        return isValidLicense(license);
    }

    public boolean isLicensed() {
        Option<PluginLicense> license = pluginLicenseManager.getLicense();
        if(license == null) {
        	return false;
        }else {
        return true;
        }
    }
}
